import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Fatura } from 'src/models/fatura';

@Injectable({
  providedIn: 'root'
})
export class DadosFaturaService {

  private readonly URL_API = 'https://desafio-it-server.herokuapp.com/lancamentos';

  constructor(
    private http: HttpClient
  ) { }

  list() {
    return this.http.get<Fatura[]>(this.URL_API);
  }
}
