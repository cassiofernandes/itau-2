import { TestBed } from '@angular/core/testing';

import { DadosFaturaService } from './dados-fatura.service';

describe('DadosFaturaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DadosFaturaService = TestBed.get(DadosFaturaService);
    expect(service).toBeTruthy();
  });
});
