import { Component, OnInit, TemplateRef } from "@angular/core";
import { DadosFaturaService } from "../dados-fatura.service";
import { Fatura } from "src/models/fatura";
import { Subscription } from "rxjs";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
  informacaoFatura: Fatura[];

  sub: Subscription;

  modalRef: BsModalRef;

  public jan: string = 'Janeiro';
  public fev: string = 'Fevereiro';
  public mar: string = 'Março';
  public abr: string = 'Abril';
  public mai: string = 'Maio';
  public jun: string = 'Junho';
  public jul: string = 'Julho';
  public ago: string = 'Agosto';
  public set: string = 'Setembro';
  public out: string = 'Outubro';
  public nov: string = 'Novembro';
  public dez: string = 'Dezembro';

  public gastoJaneiro: number;
  public gastoFevereiro: number;
  public gastoMarco: number;
  public gastoAbril: number;
  public gastoMaio: number;
  public gastoJunho: number;
  public gastoJulho: number;
  public gastoAgosto: number;
  public gastoSetembro: number;
  public gastoOutubro: number;
  public gastoNovembro: number;
  public gastoDezembro: number;


  constructor(
    private service: DadosFaturaService,
    private modalService: BsModalService
    ) {}

  ngOnInit() {
    this.listaLancamentoFatura();
  }

  listaLancamentoFatura() {
    this.sub = this.service.list()
    .subscribe((res) => {
      this.informacaoFatura = res;
      this.ordenaLista(res);
      this.somaGastosMeses(res);
    },
    err => console.log('HTTP Error', err)
    );
  }


  somaGastosMeses(lista) {
    let ja = lista.filter(res => res['mes_lancamento'] === 1);
    let fe = lista.filter(res => res['mes_lancamento'] === 2);
    let mar = lista.filter(res => res['mes_lancamento'] === 3);
    let ma = lista.filter(res => res['mes_lancamento'] === 5);
    let ju = lista.filter(res => res['mes_lancamento'] === 6);
    let jul = lista.filter(res => res['mes_lancamento'] === 7);

    this.somaItem(ja, fe, mar, ma, ju, jul);
      //console.log('filtro', filtro)

      
  }

  somaItem(j, f, m, mm, ju, jul) {
    let calcJ = j.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcF = f.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcM = m.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcMa = mm.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcJu = ju.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    let calcJul = jul.reduce((sum, valores) => {
      return sum + valores.valor;
    }, 0);

    this.gastoJaneiro = calcJ.toFixed(2);
    this.gastoFevereiro  = calcF.toFixed(2);
    this.gastoMarco  = calcM.toFixed(2);
    this.gastoMaio  = calcMa.toFixed(2);
    this.gastoJunho = calcJu.toFixed(2);
    return this.gastoJulho = calcJul.toFixed(2);
  }

  ordenaLista(listObjeto) {
    console.log(listObjeto, "listObjeto");
    listObjeto.sort(function (a, b) {
      return a.mes_lancamento < b.mes_lancamento
        ? -1
        : a.mes_lancamento > b.nome
        ? 1
        : 0;
    });
  }

  consolidado(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
